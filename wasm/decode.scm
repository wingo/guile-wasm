;; guile-wasm
;; Copyright (C) 2019 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; This library is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this library; if not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A WebAssembly toolkit for Guile.
;;
;;; Code:

(define-module (wasm decode)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 exceptions)
  #:use-module (rnrs bytevectors)
  #:use-module (wasm exceptions)
  #:use-module (wasm types)
  #:export (decode))

(define (decode port)
  (define (port-position port) (seek port 0 SEEK_CUR))

  (define (make-parse-exception message)
    (make-exception (make-parse-error)
                    (make-exception-with-message message)
                    (make-exception-with-origin (port-filename port))
                    (make-exception-with-offset (port-position port))))

  (define (parse-error message)
    (raise-exception (make-parse-exception message)))
  (define (bad-value message val)
    (raise-exception (make-exception (make-parse-exception message)
                                     (make-exception-with-irritants (list val)))))

  (define (peek-u8) (lookahead-u8 port))
  (define (read-u8)
    (let ((u8 (get-u8 port)))
      (when (eof-object? u8)
        (parse-error "unexpected end of stream"))
      u8))
  (define (eof?) (eof-object? (peek-u8)))

  (define (read-uint)
    ;; Unrolled by one.
    (let ((b (read-u8)))
      (if (zero? (logand b #x80))
          b
          (let lp ((n (logxor #x80 b)) (shift 7))
            (let ((b (read-u8)))
              (if (zero? (logand b #x80))
                  (logior (ash b shift) n)
                  (lp (logior (ash (logxor #x80 b) shift) n)
                      (+ shift 7))))))))

  (define (read-int)
    (let lp ((n 0) (shift 0))
      (let ((b (read-u8)))
        (if (zero? (logand b #x80))
            (logior (ash b shift) n
                    (if (zero? (logand #x40 b))
                        0
                        (- (ash 1 (+ shift 7)))))
            (lp (logior (ash (logxor #x80 b) shift) n)
                (+ shift 7))))))

  (define (read-bytes count)
    (let ((bytes (get-bytevector-n port count)))
      (unless (= (bytevector-length bytes) count)
        (parse-error "unexpected end of stream"))
      bytes))

  (define (read-f32)
    (bytevector-ieee-single-ref (read-bytes 4) 0 (endianness little)))
  (define (read-f64)
    (bytevector-ieee-double-ref (read-bytes 8) 0 (endianness little)))

  (define (read-var) (read-uint))
  (define (read-length) (read-uint))
  (define (read-bytevector) (read-bytes (read-length)))
  (define (read-string) (utf8->string (read-bytevector)))
  (define (read-list f)
    (let lp ((len (read-length)))
      (if (zero? len)
          '()
          (cons (f) (lp (1- len))))))

  (define (read-value-type)
    (match (read-int)
      (-#x01 'i32)
      (-#x02 'i64)
      (-#x03 'f32)
      (-#x04 'f64)
      (t (bad-value "invalid value type" t))))

  (define (read-elem-type)
    (match (read-int)
      (-#x10 'func-ref)
      (t (bad-value "invalid element type" t))))

  (define (read-result-type)
    (read-list read-value-type))
  (define (read-func-type)
    (match (read-u8)
      (#x60 (let* ((params (read-list read-value-type))
                   (results (read-list read-value-type)))
              (<- func-type params results)))
      (u8 (bad-value "while reading function type" u8))))

  (define (read-limits)
    (match (read-u8)
      (#x00 (<- limits (read-uint) #f))
      (#x01 (let* ((lo (read-uint)) (hi (read-uint)))
              (<- limits lo hi)))
      (u8 (bad-value "while reading limits" u8))))

  (define (read-table-type)
    (let* ((type (read-elem-type))
           (limits (read-limits)))
      (<- table-type limits type)))

  (define (read-memory-type)
    (<- memory-type (read-limits)))

  (define (read-global-type)
    (let* ((type (read-value-type))
           (mutable? (match (read-u8)
                       (0 #f)
                       (1 #t)
                       (u8 (bad-value "mutability" u8)))))
      (<- global-type type mutable?)))

  (define (read-memarg)
    (let* ((align (read-uint))
           (offset (read-uint)))
      (<- memarg align offset)))

  (define (read-block-type)
    (match (peek-u8)
      (#x40
       (read-u8)
       #f)
      ((? (lambda (x) (= #x40 (logand x #xc0))))
       (read-value-type))
      (else
       (let ((var (read-int)))
         (unless (<= var 0)
           (bad-value "non-negative reference" var))
         var))))

  (define (read-misc-instr)
    (parse-error "misc instructions unimplemented"))
  (define (read-thread-instr)
    (parse-error "thread instructions unimplemented"))
  (define (read-private-instr)
    (parse-error "private instructions unimplemented"))

  (define (read-instr)
    (match (read-u8)
      (#x00 (<- $unreachable))
      (#x01 (<- $nop))

      (#x02 (let* ((type (read-block-type))
                   (body (read-instrs-until-end)))
              (<- $block type body)))
      (#x03 (let* ((type (read-block-type))
                   (body (read-instrs-until-end)))
              (<- $loop type body)))
      (#x04 (let* ((type (read-block-type))
                   (then (read-instrs-until
                          (lambda ()
                            (match (peek-u8)
                              (#x05 #t)
                              (#x0b #t)
                              (_ #f)))))
                   (else (read-instrs-until-end)))
              (<- $if type then else)))

      (#x05 (parse-error "misplaced 'else' opcode"))
      (#x0b (parse-error "misplaced 'end' opcode"))

      (#x0c (<- $br (read-var)))
      (#x0d (<- $br-if (read-var)))
      (#x0e (let* ((targets (read-list read-var))
                   (default (read-var)))
              (<- $br-table targets default)))
      (#x0f (<- $return))

      (#x10 (<- $call (read-var)))
      (#x11 (let ((target (read-var)))
              (match (read-u8)
                (#x00 
                 (<- $call-indirect target))
                (u8 (bad-value "call-indirect table" u8)))))

      (#x1a (<- $drop))
      (#x1b (<- $select 'numeric))
      (#x1c (<- $select 'typed))

      (#x20 (<- $local 'get (read-var)))
      (#x21 (<- $local 'set (read-var)))
      (#x22 (<- $local 'tee (read-var)))
      (#x23 (<- $global 'get (read-var)))
      (#x24 (<- $global 'set (read-var)))
      (#x25 (<- $table 'get (read-var)))
      (#x26 (<- $table 'set (read-var)))

      (#x28 (<- $int-memory-op 'i32 'load (read-memarg)))
      (#x29 (<- $int-memory-op 'i64 'load (read-memarg)))
      (#x2a (<- $float-memory-op 'f32 'load (read-memarg)))
      (#x2b (<- $float-memory-op 'f64 'load (read-memarg)))
      (#x2c (<- $int-memory-op 'i32 'load8_s (read-memarg)))
      (#x2d (<- $int-memory-op 'i32 'load8_u (read-memarg)))
      (#x2e (<- $int-memory-op 'i32 'load16_s (read-memarg)))
      (#x2f (<- $int-memory-op 'i32 'load16_u (read-memarg)))
      (#x30 (<- $int-memory-op 'i64 'load8_s (read-memarg)))
      (#x31 (<- $int-memory-op 'i64 'load8_u (read-memarg)))
      (#x32 (<- $int-memory-op 'i64 'load16_s (read-memarg)))
      (#x33 (<- $int-memory-op 'i64 'load16_u (read-memarg)))
      (#x34 (<- $int-memory-op 'i64 'load32_s (read-memarg)))
      (#x35 (<- $int-memory-op 'i64 'load32_u (read-memarg)))

      (#x36 (<- $int-memory-op 'i32 'store (read-memarg)))
      (#x37 (<- $int-memory-op 'i64 'store (read-memarg)))
      (#x38 (<- $float-memory-op 'f32 'store (read-memarg)))
      (#x39 (<- $float-memory-op 'f64 'store (read-memarg)))
      (#x3a (<- $int-memory-op 'i32 'store8 (read-memarg)))
      (#x3b (<- $int-memory-op 'i32 'store16 (read-memarg)))
      (#x3c (<- $int-memory-op 'i32 'store8 (read-memarg)))
      (#x3d (<- $int-memory-op 'i32 'store16 (read-memarg)))
      (#x3e (<- $int-memory-op 'i32 'store32 (read-memarg)))

      (#x3f (match (read-u8)
              (#x00 (<- $memory-size))
              (u8 (bad-value "zero flag expected" u8))))
      (#x40 (match (read-u8)
              (#x00 (<- $memory-grow))
              (u8 (bad-value "zero flag expected" u8))))

      (#x41 (<- $i32-const (read-int)))
      (#x42 (<- $i64-const (read-int)))
      (#x43 (<- $f32-const (read-f32)))
      (#x44 (<- $f64-const (read-f64)))

      (#x45 (<- $test-int-op 'eqz 'i32))
      (#x46 (<- $compare-int-op 'eq 'i32))
      (#x47 (<- $compare-int-op 'ne 'i32))
      (#x48 (<- $compare-int-op 'lt_s 'i32))
      (#x49 (<- $compare-int-op 'lt_u 'i32))
      (#x4a (<- $compare-int-op 'gt_s 'i32))
      (#x4b (<- $compare-int-op 'gt_u 'i32))
      (#x4c (<- $compare-int-op 'le_s 'i32))
      (#x4d (<- $compare-int-op 'le_u 'i32))
      (#x4e (<- $compare-int-op 'ge_s 'i32))
      (#x4f (<- $compare-int-op 'ge_u 'i32))

      (#x50 (<- $test-int-op 'eqz 'i64))
      (#x51 (<- $compare-int-op 'eq 'i64))
      (#x52 (<- $compare-int-op 'ne 'i64))
      (#x53 (<- $compare-int-op 'lt_s 'i64))
      (#x54 (<- $compare-int-op 'lt_u 'i64))
      (#x55 (<- $compare-int-op 'gt_s 'i64))
      (#x56 (<- $compare-int-op 'gt_u 'i64))
      (#x57 (<- $compare-int-op 'le_s 'i64))
      (#x58 (<- $compare-int-op 'le_u 'i64))
      (#x59 (<- $compare-int-op 'ge_s 'i64))
      (#x5a (<- $compare-int-op 'ge_u 'i64))

      (#x5b (<- $compare-float-op 'eq 'f32))
      (#x5c (<- $compare-float-op 'ne 'f32))
      (#x5d (<- $compare-float-op 'lt 'f32))
      (#x5e (<- $compare-float-op 'gt 'f32))
      (#x5f (<- $compare-float-op 'le 'f32))
      (#x60 (<- $compare-float-op 'ge 'f32))

      (#x61 (<- $compare-float-op 'eq 'f64))
      (#x62 (<- $compare-float-op 'ne 'f64))
      (#x63 (<- $compare-float-op 'lt 'f64))
      (#x64 (<- $compare-float-op 'gt 'f64))
      (#x65 (<- $compare-float-op 'le 'f64))
      (#x66 (<- $compare-float-op 'ge 'f64))

      (#x67 (<- $unary-int-op 'clz 'i32))
      (#x68 (<- $unary-int-op 'ctz 'i32))
      (#x69 (<- $unary-int-op 'popcnt 'i32))
      (#x6a (<- $binary-int-op 'add 'i32))
      (#x6b (<- $binary-int-op 'sub 'i32))
      (#x6c (<- $binary-int-op 'mul 'i32))
      (#x6d (<- $binary-int-op 'div_s 'i32))
      (#x6e (<- $binary-int-op 'div_u 'i32))
      (#x6f (<- $binary-int-op 'rem_s 'i32))
      (#x70 (<- $binary-int-op 'rem_u 'i32))
      (#x71 (<- $binary-int-op 'and 'i32))
      (#x72 (<- $binary-int-op 'or 'i32))
      (#x73 (<- $binary-int-op 'xor 'i32))
      (#x74 (<- $binary-int-op 'shl 'i32))
      (#x75 (<- $binary-int-op 'shr_s 'i32))
      (#x76 (<- $binary-int-op 'shr_u 'i32))
      (#x77 (<- $binary-int-op 'rotl 'i32))
      (#x78 (<- $binary-int-op 'rotr 'i32))

      (#x79 (<- $unary-int-op 'clz 'i64))
      (#x7a (<- $unary-int-op 'ctz 'i64))
      (#x7b (<- $unary-int-op 'popcnt 'i64))
      (#x7c (<- $binary-int-op 'add 'i64))
      (#x7d (<- $binary-int-op 'sub 'i64))
      (#x7e (<- $binary-int-op 'mul 'i64))
      (#x7f (<- $binary-int-op 'div_s 'i64))
      (#x80 (<- $binary-int-op 'div_u 'i64))
      (#x81 (<- $binary-int-op 'rem_s 'i64))
      (#x82 (<- $binary-int-op 'rem_u 'i64))
      (#x83 (<- $binary-int-op 'and 'i64))
      (#x84 (<- $binary-int-op 'or 'i64))
      (#x85 (<- $binary-int-op 'xor 'i64))
      (#x86 (<- $binary-int-op 'shl 'i64))
      (#x87 (<- $binary-int-op 'shr_s 'i64))
      (#x88 (<- $binary-int-op 'shr_u 'i64))
      (#x89 (<- $binary-int-op 'rotl 'i64))
      (#x8a (<- $binary-int-op 'rotr 'i64))

      (#x8b (<- $unary-float-op 'abs 'f32))
      (#x8c (<- $unary-float-op 'neg 'f32))
      (#x8d (<- $unary-float-op 'ceil 'f32))
      (#x8e (<- $unary-float-op 'floor 'f32))
      (#x8f (<- $unary-float-op 'trunc 'f32))
      (#x90 (<- $unary-float-op 'nearest 'f32))
      (#x91 (<- $unary-float-op 'sqrt 'f32))
      (#x92 (<- $binary-float-op 'add 'f32))
      (#x93 (<- $binary-float-op 'sub 'f32))
      (#x94 (<- $binary-float-op 'mul 'f32))
      (#x95 (<- $binary-float-op 'div 'f32))
      (#x96 (<- $binary-float-op 'min 'f32))
      (#x97 (<- $binary-float-op 'max 'f32))
      (#x98 (<- $binary-float-op 'copysign 'f32))

      (#x99 (<- $unary-float-op 'abs 'f64))
      (#x9a (<- $unary-float-op 'neg 'f64))
      (#x9b (<- $unary-float-op 'ceil 'f64))
      (#x9c (<- $unary-float-op 'floor 'f64))
      (#x9d (<- $unary-float-op 'trunc 'f64))
      (#x9e (<- $unary-float-op 'nearest 'f64))
      (#x9f (<- $unary-float-op 'sqrt 'f64))
      (#xa0 (<- $binary-float-op 'add 'f64))
      (#xa1 (<- $binary-float-op 'sub 'f64))
      (#xa2 (<- $binary-float-op 'mul 'f64))
      (#xa3 (<- $binary-float-op 'div 'f64))
      (#xa4 (<- $binary-float-op 'min 'f64))
      (#xa5 (<- $binary-float-op 'max 'f64))
      (#xa6 (<- $binary-float-op 'copysign 'f64))

      (#xa7 (<- $convert-int-op 'wrap_i64 'i32))
      (#xa8 (<- $convert-int-op 'trunc_f32_s 'i32))
      (#xa9 (<- $convert-int-op 'trunc_f32_u 'i32))
      (#xaa (<- $convert-int-op 'trunc_f64_s 'i32))
      (#xab (<- $convert-int-op 'trunc_f64_u 'i32))
      (#xac (<- $convert-int-op 'extend_i32_s 'i64))
      (#xad (<- $convert-int-op 'extend_i32_u 'i64))
      (#xae (<- $convert-int-op 'trunc_f32_s 'i64))
      (#xaf (<- $convert-int-op 'trunc_f32_u 'i64))
      (#xb0 (<- $convert-int-op 'trunc_f64_s 'i64))
      (#xb1 (<- $convert-int-op 'trunc_f64_u 'i64))
      (#xb2 (<- $convert-float-op 'convert_i32_s 'f32))
      (#xb3 (<- $convert-float-op 'convert_i32_u 'f32))
      (#xb4 (<- $convert-float-op 'convert_i64_s 'f32))
      (#xb5 (<- $convert-float-op 'convert_i64_u 'f32))
      (#xb6 (<- $convert-float-op 'demote_f64 'f32))
      (#xb7 (<- $convert-float-op 'convert_i32_s 'f64))
      (#xb8 (<- $convert-float-op 'convert_i32_u 'f64))
      (#xb9 (<- $convert-float-op 'convert_i64_s 'f64))
      (#xba (<- $convert-float-op 'convert_i64_u 'f64))
      (#xbb (<- $convert-float-op 'promote_f32 'f64))

      (#xbc (<- $convert-int-op 'reinterpret_float 'i32))
      (#xbd (<- $convert-int-op 'reinterpret_float 'i64))
      (#xbe (<- $convert-float-op 'reinterpret_int 'f32))
      (#xbf (<- $convert-float-op 'reinterpret_int 'f64))

      ;; Sign extension proposal.
      (#xc0 (<- $convert-int-op 'extend8_s 'i32))
      (#xc1 (<- $convert-int-op 'extend16_s 'i32))
      (#xc2 (<- $convert-int-op 'extend8_s 'i64))
      (#xc3 (<- $convert-int-op 'extend16_s 'i64))
      (#xc4 (<- $convert-int-op 'extend32_s 'i64))

      (#xfc (read-misc-instr))
      (#xfe (read-thread-instr))
      (#xff (read-private-instr))

      (u8 (bad-value "unexpected byte" u8))))

  (define (read-instrs-until pred)
    (if (pred)
        '()
        (cons (read-instr) (read-instrs-until pred))))
  (define (read-instrs-until-end)
    (read-instrs-until (lambda ()
                         (match (peek-u8)
                           (#x0b (read-u8))
                           (_ #f)))))

  (define (peek-section-id)
    (match (peek-u8)
      ((? eof-object?) #f)
      (0 'custom)
      (1 'types)
      (2 'imports)
      (3 'funcs)
      (4 'tables)
      (5 'memories)
      (6 'globals)
      (7 'exports)
      (8 'start)
      (9 'elems)
      (10 'code)
      (11 'data)
      (u8 (bad-value "section id" u8))))

  (define (read/sized f)
    (let* ((len (read-uint))
           (pos (port-position port))
           (res (f len)))
      (unless (= (port-position port) (+ pos len))
        (parse-error "section size mismatch"))
      res))

  (define (read-custom-section)
    (read/sized read-bytes))

  (define (ignore-custom-sections-and-then f)
    (match (peek-section-id)
      ('custom
       (read-u8)
       (read-custom-section)
       (ignore-custom-sections-and-then f))
      (_ (f))))

  (define (read-section kind kt kf)
    (ignore-custom-sections-and-then
     (lambda ()
       (cond
        ((eq? (peek-section-id) kind)
         (read-u8)
         (read/sized kt))
        (else (kf))))))

  (define (read-list-section kind read)
    (read-section kind
                  (lambda (len) (read-list read))
                  (lambda () '())))

  (define-syntax-rule (read-module (name (f arg ...)) ... expr)
    (let* ((name (f 'name arg ...))
           ...)
      expr))

  (match (read-bytes 4)
    (#vu8(#x00 #x61 #x73 #x6d) #t)
    (bytes (bad-value "bad magic" bytes)))
  (match (read-bytes 4)
    (#vu8(1 0 0 0) #t)
    (version (bad-value "bad version" version)))    

  (read-module
   (types
    (read-list-section read-func-type))
   (imports
    (read-list-section
     (lambda ()
       (let* ((module (read-string))
              (name (read-string))
              (desc (match (read-u8)
                      (#x00 (<- func-import (read-var)))
                      (#x01 (<- table-import (read-table-type)))
                      (#x02 (<- memory-import (read-memory-type)))
                      (#x03 (<- global-import (read-memory-type)))
                      (u8 (bad-value "import type" u8)))))
         (<- import module name desc)))))
   
   (funcs
    (read-list-section read-var))
   (tables
    (read-list-section
     (lambda ()
       (<- table (read-table-type)))))
   (memories
    (read-list-section
     (lambda ()
       (<- memory (read-memory-type)))))
   (globals
    (read-list-section
     (lambda ()
       (let* ((type (read-global-type))
              (value (read-instrs-until-end)))
         (<- global type value)))))
   (exports
    (read-list-section
     (lambda ()
       (let* ((name (read-string))
              (desc (match (read-u8)
                      (#x00 (<- func-export (read-var)))
                      (#x01 (<- table-export (read-var)))
                      (#x02 (<- memory-export (read-var)))
                      (#x03 (<- global-export (read-var)))
                      (u8 (bad-value "export type" u8)))))
         (<- export name desc)))))
   (start
    (read-section
     (lambda (len) (read-var))
     (lambda () #f)))
   (elems
    (read-list-section
     (lambda ()
       (let* ((index (read-var))
              (offset (read-instrs-until-end))
              (init (read-list read-var)))
         (<- elem index offset init)))))
   (code
    (read-list-section
     (lambda ()
       (read/sized
        (lambda (len)
          (let* ((locals (let lp ((n (read-length)))
                           (if (zero? n)
                               '()
                               (let* ((count (read-length))
                                      (type (read-value-type)))
                                 (append (make-list count type) (lp (1- n)))))))
                 (body (read-instrs-until-end)))
            (cons locals body)))))))
   (data
    (read-list-section
     (lambda ()
       (let* ((index (read-var))
              (offset (read-instrs-until-end))
              (init (read-bytevector)))
         (<- data index offset init)))))

   (ignore-custom-sections-and-then
    (lambda ()
      (unless (eof?)
        (parse-error "junk at end of module"))
      (let ((funcs (map (lambda (type locals body)
                          (<- func type locals body))
                        funcs (map car code) (map cdr code))))
        (<- module types globals tables memories funcs
            start elems data imports exports))))))
