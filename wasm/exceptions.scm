;; guile-wasm
;; Copyright (C) 2019 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; This library is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this library; if not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A WebAssembly toolkit for Guile.
;;
;;; Code:

(define-module (wasm exceptions)
  #:use-module (ice-9 exceptions)
  #:export (&parse-error
            make-parse-error parse-error?
            &offset
            make-exception-with-offset exception-with-offset?
            exception-offset))

(define-exception-type &parse-error &external-error
  make-parse-error parse-error?)

(define-exception-type &offset &exception
  make-exception-with-offset exception-with-offset?
  (offset exception-offset))
