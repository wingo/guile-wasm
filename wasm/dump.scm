;; guile-wasm
;; Copyright (C) 2019 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; This library is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this library; if not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A one-pass dumper for WebAssembly files.  Useful even if the tail of
;; the file is invalid in some way.
;;
;;; Code:

(define-module (wasm dump)
  #:use-module (ice-9 match)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 exceptions)
  #:use-module (rnrs bytevectors)
  #:use-module (wasm exceptions)
  #:export (dump))

(define* (dump port #:optional (out (current-output-port)))
  (define (port-position port) (seek port 0 SEEK_CUR))

  (define (make-parse-exception message)
    (force-output out)
    (make-exception (make-parse-error)
                    (make-exception-with-message message)
                    (make-exception-with-origin (port-filename port))
                    (make-exception-with-offset (port-position port))))

  (define (parse-error message)
    (raise-exception (make-parse-exception message)))
  (define (bad-value message val)
    (raise-exception (make-exception (make-parse-exception message)
                                     (make-exception-with-irritants (list val)))))

  (define (peek-u8) (lookahead-u8 port))
  (define (read-u8)
    (let ((u8 (get-u8 port)))
      (when (eof-object? u8)
        (parse-error "unexpected end of stream"))
      u8))
  (define (eof?) (eof-object? (peek-u8)))

  (define (read-uint)
    ;; Unrolled by one.
    (let ((b (read-u8)))
      (if (zero? (logand b #x80))
          b
          (let lp ((n (logxor #x80 b)) (shift 7))
            (let ((b (read-u8)))
              (if (zero? (logand b #x80))
                  (logior (ash b shift) n)
                  (lp (logior (ash (logxor #x80 b) shift) n)
                      (+ shift 7))))))))

  (define (read-int)
    (let lp ((n 0) (shift 0))
      (let ((b (read-u8)))
        (if (zero? (logand b #x80))
            (logior (ash b shift) n
                    (if (zero? (logand #x40 b))
                        0
                        (- (ash 1 (+ shift 7)))))
            (lp (logior (ash (logxor #x80 b) shift) n)
                (+ shift 7))))))

  (define (read-bytes count)
    (let ((bytes (get-bytevector-n port count)))
      (unless (= (bytevector-length bytes) count)
        (parse-error "unexpected end of stream"))
      bytes))

  (define (dump x) (write x out))
  (define (dump-char x) (put-char out x))
  (define (dump-chars x) (put-string out x))

  (define (spc) (dump-char #\space))
  (define (lpar) (dump-char #\())
  (define (rpar) (dump-char #\)))
  (define (inline-pars f) (lpar) (f) (rpar))
  (define (indent n) (unless (zero? n) (spc) (indent (1- n))))
  (define (newline-and-indent n) (dump-char #\newline) (indent n))

  (define (read-f32)
    (bytevector-ieee-single-ref (read-bytes 4) 0 (endianness little)))
  (define (read-f64)
    (bytevector-ieee-double-ref (read-bytes 8) 0 (endianness little)))

  (define (dump-var) (dump (read-uint)))
  (define (read-length) (read-uint))
  (define (read-bytevector) (read-bytes (read-length)))
  (define (dump-string) (dump (utf8->string (read-bytevector))))
  (define (list-for-each/infix infix f)
    (let ((len (read-length)))
      (let lp ((n len))
        (unless (zero? n)
          (unless (eq? n len)
            (infix))
          (f)
          (lp (1- n))))))
  (define (list-for-each/prefix prefix f)
    (list-for-each/infix values (lambda () (prefix) (f))))

  (define (dump-tagged tag f . args)
    (lpar) (dump tag) (spc) (apply f args) (rpar))
  (define (tagged-dumper tag f . args)
    (lambda () (apply dump-tagged tag f args)))

  (define (dump-typeref)
    (dump-tagged 'type dump-var))

  (define (read-value-type)
    (match (read-int)
      (-#x01 'i32)
      (-#x02 'i64)
      (-#x03 'f32)
      (-#x04 'f64)
      (t (bad-value "invalid value type" t))))
  (define (dump-value-type)
    (dump (read-value-type)))

  (define (read-elem-type)
    (match (read-int)
      (-#x10 'func-ref)
      (t (bad-value "invalid element type" t))))

  (define (dump-result-type kind)
    (dump-tagged kind list-for-each/infix spc dump-value-type))
  (define (dump-func-type)
    (dump-tagged
     'func
     (lambda ()
       (match (read-u8)
         (#x60
          (dump-result-type 'param)
          (spc)
          (dump-result-type 'result))
         (u8 (bad-value "while reading function type" u8))))))

  (define (read-limits)
    (match (read-u8)
      (#x00 (read-uint))
      (#x01 (let* ((min (read-uint)) (max (read-uint)))
              (cons min max)))
      (u8 (bad-value "while reading limits" u8))))
  (define (dump-limits limits)
    (match limits
      ((min . max) (dump min) (spc) (dump max))
      (min (dump min))))

  (define (dump-table-type)
    (let* ((elem-type (read-elem-type))
           (limits (read-limits)))
      (dump elem-type)
      (dump-limits limits)))

  (define (dump-memory-type)
    (dump-limits (read-limits)))

  (define (dump-global-type)
    (let* ((type (read-value-type))
           (mutable? (match (read-u8)
                       (0 #f)
                       (1 #t)
                       (u8 (bad-value "mutability" u8)))))
      (cond
       (mutable?
        (dump-tagged 'mut dump type))
       (else
        (dump type)))))

  (define (dump-memarg)
    (let* ((align (read-uint))
           (offset (read-uint)))
      (dump offset) (spc) (dump align)))

  (define (dump-block-type)
    (match (peek-u8)
      (#x40
       (read-u8))
      ((? (lambda (x) (= #x40 (logand x #xc0))))
       (dump-tagged 'result dump-value-type))
      (else
       (dump-typeref))))

  (define (dump-misc-instr)
    (parse-error "misc instructions unimplemented"))
  (define (dump-thread-instr)
    (parse-error "thread instructions unimplemented"))
  (define (dump-private-instr)
    (parse-error "private instructions unimplemented"))

  (define (dump-instr indent)
    (newline-and-indent indent)
    (match (read-u8)
      (#x00 (dump 'unreachable))
      (#x01 (dump 'nop))

      (#x02
       (lpar) (dump-chars "block ") (dump-block-type)
       (dump-instrs-until-end (+ indent 2))
       (rpar))
      (#x03
       (lpar) (dump-chars "loop ") (dump-block-type)
       (dump-instrs-until-end (+ indent 2))
       (rpar))
      (#x04
       (lpar) (dump-chars "if ") (dump-block-type)
       (newline-and-indent (+ indent 1))
       (lpar) (dump-chars "then")
       (dump-instrs-until
        (lambda ()
          (match (peek-u8)
            (#x05
             (rpar)
             (newline-and-indent (+ indent 1))
             (lpar) (dump-chars "else"))
            (#x0b #t)
            (_ #f)))
        (+ indent 3))
       (dump-instrs-until-end (+ indent 3))
       (rpar) (rpar))

      (#x05 (parse-error "misplaced 'else' opcode"))
      (#x0b (parse-error "misplaced 'end' opcode"))

      (#x0c (dump-tagged 'br dump-var))
      (#x0d (dump-tagged 'br_if dump-var))
      (#x0e
       (lpar) (dump 'br_table)
       (list-for-each/prefix spc dump-var)
       (spc) (dump-var) (rpar))
      (#x0f (dump 'return))

      (#x10 (dump-tagged 'call dump-var))
      (#x11 (dump-tagged 'call_indirect dump-var)
            (match (read-u8)
              (#x00 #t)
              (u8 (bad-value "call-indirect table" u8))))

      (#x1a (dump 'drop))
      (#x1b (dump 'select))
      (#x1c (dump 'select_typed))

      (#x20 (dump-tagged 'local.get dump-var))
      (#x21 (dump-tagged 'local.set dump-var))
      (#x22 (dump-tagged 'local.tee dump-var))
      (#x23 (dump-tagged 'global.get dump-var))
      (#x24 (dump-tagged 'global.set dump-var))
      (#x25 (dump-tagged 'table.get dump-var))
      (#x26 (dump-tagged 'table.set dump-var))

      (#x28 (dump-tagged 'i32.load dump-memarg))
      (#x29 (dump-tagged 'i64.load dump-memarg))
      (#x2a (dump-tagged 'f32.load dump-memarg))
      (#x2b (dump-tagged 'f64.load dump-memarg))
      (#x2c (dump-tagged 'i32.load8_s dump-memarg))
      (#x2d (dump-tagged 'i32.load8_u dump-memarg))
      (#x2e (dump-tagged 'i32.load16_s dump-memarg))
      (#x2f (dump-tagged 'i32.load16_u dump-memarg))
      (#x30 (dump-tagged 'i64.load8_s dump-memarg))
      (#x31 (dump-tagged 'i64.load8_u dump-memarg))
      (#x32 (dump-tagged 'i64.load16_s dump-memarg))
      (#x33 (dump-tagged 'i64.load16_u dump-memarg))
      (#x34 (dump-tagged 'i64.load32_s dump-memarg))
      (#x35 (dump-tagged 'i64.load32_u dump-memarg))

      (#x36 (dump-tagged 'i32.store dump-memarg))
      (#x37 (dump-tagged 'i64.store dump-memarg))
      (#x38 (dump-tagged 'f32.store dump-memarg))
      (#x39 (dump-tagged 'f64.store dump-memarg))
      (#x3a (dump-tagged 'i32.store8 dump-memarg))
      (#x3b (dump-tagged 'i32.store16 dump-memarg))
      (#x3c (dump-tagged 'i32.store8 dump-memarg))
      (#x3d (dump-tagged 'i32.store16 dump-memarg))
      (#x3e (dump-tagged 'i32.store32 dump-memarg))

      (#x3f (match (read-u8)
              (#x00 (dump 'memory.size))
              (u8 (bad-value "zero flag expected" u8))))
      (#x40 (match (read-u8)
              (#x00 (dump 'memory.grow))
              (u8 (bad-value "zero flag expected" u8))))

      (#x41 (dump-tagged 'i32.const dump (read-int)))
      (#x42 (dump-tagged 'i64.const dump (read-int)))
      (#x43 (dump-tagged 'f32.const dump (read-f32)))
      (#x44 (dump-tagged 'f64.const dump (read-f64)))

      (#x45 (dump 'i32.eqz))
      (#x46 (dump 'i32.eq))
      (#x47 (dump 'i32.ne))
      (#x48 (dump 'i32.lt_s))
      (#x49 (dump 'i32.lt_u))
      (#x4a (dump 'i32.gt_s))
      (#x4b (dump 'i32.gt_u))
      (#x4c (dump 'i32.le_s))
      (#x4d (dump 'i32.le_u))
      (#x4e (dump 'i32.ge_s))
      (#x4f (dump 'i32.ge_u))

      (#x50 (dump 'i64.eqz))
      (#x51 (dump 'i64.eq))
      (#x52 (dump 'i64.ne))
      (#x53 (dump 'i64.lt_s))
      (#x54 (dump 'i64.lt_u))
      (#x55 (dump 'i64.gt_s))
      (#x56 (dump 'i64.gt_u))
      (#x57 (dump 'i64.le_s))
      (#x58 (dump 'i64.le_u))
      (#x59 (dump 'i64.ge_s))
      (#x5a (dump 'i64.ge_u))

      (#x5b (dump 'f32.eq))
      (#x5c (dump 'f32.ne))
      (#x5d (dump 'f32.lt))
      (#x5e (dump 'f32.gt))
      (#x5f (dump 'f32.le))
      (#x60 (dump 'f32.ge))

      (#x61 (dump 'f64.eq))
      (#x62 (dump 'f64.ne))
      (#x63 (dump 'f64.lt))
      (#x64 (dump 'f64.gt))
      (#x65 (dump 'f64.le))
      (#x66 (dump 'f64.ge))

      (#x67 (dump 'i32.clz))
      (#x68 (dump 'i32.ctz))
      (#x69 (dump 'i32.popcnt))
      (#x6a (dump 'i32.add))
      (#x6b (dump 'i32.sub))
      (#x6c (dump 'i32.mul))
      (#x6d (dump 'i32.div_s))
      (#x6e (dump 'i32.div_u))
      (#x6f (dump 'i32.rem_s))
      (#x70 (dump 'i32.rem_u))
      (#x71 (dump 'i32.and))
      (#x72 (dump 'i32.or))
      (#x73 (dump 'i32.xor))
      (#x74 (dump 'i32.shl))
      (#x75 (dump 'i32.shr_s))
      (#x76 (dump 'i32.shr_u))
      (#x77 (dump 'i32.rotl))
      (#x78 (dump 'i32.rotr))

      (#x79 (dump 'i64.clz))
      (#x7a (dump 'i64.ctz))
      (#x7b (dump 'i64.popcnt))
      (#x7c (dump 'i64.add))
      (#x7d (dump 'i64.sub))
      (#x7e (dump 'i64.mul))
      (#x7f (dump 'i64.div_s))
      (#x80 (dump 'i64.div_u))
      (#x81 (dump 'i64.rem_s))
      (#x82 (dump 'i64.rem_u))
      (#x83 (dump 'i64.and))
      (#x84 (dump 'i64.or))
      (#x85 (dump 'i64.xor))
      (#x86 (dump 'i64.shl))
      (#x87 (dump 'i64.shr_s))
      (#x88 (dump 'i64.shr_u))
      (#x89 (dump 'i64.rotl))
      (#x8a (dump 'i64.rotr))

      (#x8b (dump 'f32.abs))
      (#x8c (dump 'f32.neg))
      (#x8d (dump 'f32.ceil))
      (#x8e (dump 'f32.floor))
      (#x8f (dump 'f32.trunc))
      (#x90 (dump 'f32.nearest))
      (#x91 (dump 'f32.sqrt))
      (#x92 (dump 'f32.add))
      (#x93 (dump 'f32.sub))
      (#x94 (dump 'f32.mul))
      (#x95 (dump 'f32.div))
      (#x96 (dump 'f32.min))
      (#x97 (dump 'f32.max))
      (#x98 (dump 'f32.copysign))

      (#x99 (dump 'f64.abs))
      (#x9a (dump 'f64.neg))
      (#x9b (dump 'f64.ceil))
      (#x9c (dump 'f64.floor))
      (#x9d (dump 'f64.trunc))
      (#x9e (dump 'f64.nearest))
      (#x9f (dump 'f64.sqrt))
      (#xa0 (dump 'f64.add))
      (#xa1 (dump 'f64.sub))
      (#xa2 (dump 'f64.mul))
      (#xa3 (dump 'f64.div))
      (#xa4 (dump 'f64.min))
      (#xa5 (dump 'f64.max))
      (#xa6 (dump 'f64.copysign))

      (#xa7 (dump 'i32.wrap_i64))
      (#xa8 (dump 'i32.trunc_f32_s))
      (#xa9 (dump 'i32.trunc_f32_u))
      (#xaa (dump 'i32.trunc_f64_s))
      (#xab (dump 'i32.trunc_f64_u))
      (#xac (dump 'i64.extend_i32_s))
      (#xad (dump 'i64.extend_i32_u))
      (#xae (dump 'i64.trunc_f32_s))
      (#xaf (dump 'i64.trunc_f32_u))
      (#xb0 (dump 'i64.trunc_f64_s))
      (#xb1 (dump 'i64.trunc_f64_u))
      (#xb2 (dump 'f32.convert_i32_s))
      (#xb3 (dump 'f32.convert_i32_u))
      (#xb4 (dump 'f32.convert_i64_s))
      (#xb5 (dump 'f32.convert_i64_u))
      (#xb6 (dump 'f32.demote_f64))
      (#xb7 (dump 'f64.convert_i32_s))
      (#xb8 (dump 'f64.convert_i32_u))
      (#xb9 (dump 'f64.convert_i64_s))
      (#xba (dump 'f64.convert_i64_u))
      (#xbb (dump 'f64.promote_f32))

      (#xbc (dump 'i32.reinterpret_float))
      (#xbd (dump 'i64.reinterpret_float))
      (#xbe (dump 'f32.reinterpret_int))
      (#xbf (dump 'f64.reinterpret_int))

      ;; Sign extension proposal.
      (#xc0 (dump 'i32.extend8_s))
      (#xc1 (dump 'i32.extend16_s))
      (#xc2 (dump 'i64.extend8_s))
      (#xc3 (dump 'i64.extend16_s))
      (#xc4 (dump 'i64.extend32_s))

      (#xfc (dump-misc-instr))
      (#xfe (dump-thread-instr))
      (#xff (dump-private-instr))

      (u8 (bad-value "unexpected byte" u8))))

  (define (dump-instrs-until pred indent)
    (if (pred)
        '()
        (cons (dump-instr indent) (dump-instrs-until pred indent))))
  (define (dump-instrs-until-end indent)
    (dump-instrs-until (lambda ()
                         (match (peek-u8)
                           (#x0b (read-u8))
                           (_ #f)))
                       indent))

  (define (peek-section-id)
    (match (peek-u8)
      ((? eof-object?) #f)
      (0 'custom)
      (1 'types)
      (2 'imports)
      (3 'funcs)
      (4 'tables)
      (5 'memories)
      (6 'globals)
      (7 'exports)
      (8 'start)
      (9 'elems)
      (10 'code)
      (11 'data)
      (u8 (bad-value "section id" u8))))

  (define (dump/sized f)
    (let* ((len (read-uint))
           (pos (port-position port)))
      (f len)
      (unless (= (port-position port) (+ pos len))
        (parse-error "section size mismatch"))))

  (define (dump-custom-section)
    (dump/sized (lambda (len) (dump (read-bytes len)))))

  (define (ignore-custom-sections-and-then f)
    (match (peek-section-id)
      ('custom
       (read-u8)
       (dump-custom-section)
       (ignore-custom-sections-and-then f))
      (_ (f))))

  (define (dump-section kind kt)
    (ignore-custom-sections-and-then
     (lambda ()
       (when (eq? (peek-section-id) kind)
         (read-u8)
         (dump/sized kt)))))

  (define (dump-list-section kind tag f)
    (dump-section kind
                  (lambda (len)
                    (list-for-each/prefix
                     (lambda () (newline-and-indent 2))
                     (tagged-dumper tag f)))))

  (match (read-bytes 4)
    (#vu8(#x00 #x61 #x73 #x6d) #t)
    (bytes (bad-value "bad magic" bytes)))
  (match (read-bytes 4)
    (#vu8(1 0 0 0) #t)
    (version (bad-value "bad version" version)))

  (dump-chars "(module")

  (dump-list-section 'types 'type dump-func-type)

  (dump-list-section
   'imports
   'import
   (lambda ()
     (dump-string)
     (spc)
     (dump-string)
     (match (read-u8)
       (#x00 (dump-tagged 'func dump-typeref))
       (#x01 (dump-tagged 'table dump-table-type))
       (#x02 (dump-tagged 'memory dump-memory-type))
       (#x03 (dump-tagged 'global dump-global-type))
       (u8 (bad-value "import type" u8)))))

  ;; Our one concession to doing anything other than read-and-dump is to
  ;; slurp function types here and splat them into functions later.
  (define func-types
    (ignore-custom-sections-and-then
     (lambda ()
       (cond
        ((eq? (peek-section-id) 'funcs)
         (read-u8)
         (let* ((len (read-uint))
                (pos (port-position port))
                (types (let lp ((n (read-uint)))
                         (if (zero? n)
                             '()
                             (let ((t (read-uint)))
                               (cons t (lp (1- n))))))))
           (unless (= (port-position port) (+ pos len))
             (parse-error "section size mismatch"))
           types))
        (else '())))))

  (dump-list-section 'tables 'table dump-table-type)
  (dump-list-section 'memories 'memory dump-memory-type)

  (dump-list-section
   'globals 'global
   (lambda ()
     (dump-global-type)
     (spc)
     (dump-instrs-until-end 4)))

  (dump-list-section
   'exports 'export
   (lambda ()
     (dump-string)
     (spc)
     (dump-tagged (match (read-u8)
                    (#x00 'func)
                    (#x01 'table)
                    (#x02 'memory-export)
                    (#x03 'global-export)
                    (u8 (bad-value "export type" u8)))
                  dump-var)))
  (dump-section
   'start
   (lambda (len) (dump-tagged 'start dump-var)))
  (dump-list-section
   'elems 'elem
   (lambda ()
     (dump-var) (spc)
     (newline-and-indent 4)
     (dump-chars "(offset") (dump-instrs-until-end 6) (rpar)
     (newline-and-indent 4) (list-for-each/prefix spc dump-var)))
  (dump-list-section
   'code 'func
   (lambda ()
     (dump/sized
      (lambda (len)
        (dump-tagged 'type (lambda () (dump (car func-types)))) (spc)
        (set! func-types (cdr func-types))
        (dump-chars "(local")
        (let lp ((n (read-length)))
          (unless (zero? n)
            (let* ((count (read-length))
                   (type (read-value-type)))
              (let lp ((count count))
                (unless (zero? count)
                  (spc) (dump type))))
            (lp (1- n))))
        (rpar)
        (dump-instrs-until-end 4)))))
  (dump-list-section
   'data 'data
   (lambda ()
     (dump-var) (spc)
     (newline-and-indent 4)
     (dump-chars "(offset") (dump-instrs-until-end 6) (rpar)
     (newline-and-indent 4)
     (dump (read-bytevector))))

  (unless (eof?)
    (parse-error "junk at end of module"))

  (rpar))
