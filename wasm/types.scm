;; guile-wasm
;; Copyright (C) 2019 Andy Wingo <wingo at pobox dot com>

;; This library is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or (at
;; your option) any later version.
;;
;; This library is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this library; if not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A WebAssembly toolkit for Guile.
;;
;;; Code:

(define-module (wasm types)
  #:use-module (ice-9 match)
  #:replace (const import export)
  #:export (<-))


(define-syntax define-record
  (lambda (stx)
    (syntax-case stx (<)
      ((_ (id < parent) field ...)
       (let ((id-append (lambda args
                          (define (->symbol x)
                            (if (symbol? x) x (syntax->datum x)))
                          (datum->syntax #'id
                                         (apply symbol-append
                                                (map ->symbol args))))))
         (with-syntax (((getter ...)
                        (map (lambda (f)
                               (id-append #'id '- f))
                             #'(field ...))))
           #`(begin
               (define id
                 (make-record-type 'id '((immutable field) ...)
                                   #:parent parent #:extensible? #t))
               (define #,(id-append 'make- #'id) (record-constructor id))
               (define #,(id-append #'id '?) (record-predicate id))
               (define getter (record-accessor id 'field))
               ...))))
      ((_ id field ...)
       #'(define-record (id < #f) field ...)))))

(define-record %type constructor predicate)
(define (type-constructor t)
  (or (%type-constructor t)
      (let ((pred (%type-predicate t)))
        (lambda (x)
          (unless (pred x)
            (error "value not an instance of type" x t))
          x))))

(define (predicate p)
  (make-%type #f p))

(define (ranged-integer lo hi)
  (predicate (lambda (x)
               (and (exact-integer? x) (<= lo x hi)))))
(define (fixed-width-integer width signed?)
  (if signed?
      (ranged-integer (ash -1 (1- width)) (1- (ash 1 (1- width))))
      (ranged-integer 0 (1- (ash 1 width)))))

(define-record (%literal < %type) value)
(define (literal value)
  (make-%literal #f
                 (lambda (x) (equal? x value))
                 value))

(define-record (%list-of < %type) type)
(define (list-of type)
  (let ((pred (%type-predicate type)))
    (make-%list-of #f
                   (lambda (x) (and-map pred x))
                   type)))

(define-record (%sum < %type) types)
(define (sum types)
  (let ((preds (map %type-predicate types)))
    (make-%sum #f
               (lambda (x) (or-map (lambda (pred) (pred x)) preds))
               types)))

(define-record (%product < %type) rtd types)
(define (product rtd constructor types)
  (make-%product constructor (record-predicate rtd) rtd types))

(define-syntax type-ref
  (syntax-rules (quote or list)
    ((_ (quote x))
     (literal 'x))
    ((_ (or t ...))
     (sum (list (type-ref t) ...)))
    ((_ (list t))
     (list-of (type-ref t)))
    ((_ x)
     (let ((t x))
       (unless (%type? t)
         (error "not a type" t))
       t))))

(define-syntax-rule (define-type id t)
  (define-public id (type-ref t)))

(define-syntax define-struct-type
  (lambda (stx)
    (syntax-case stx ()
      ((_ id (name t) ...)
       (with-syntax (((pred ...) (generate-temporaries #'(name ...)))
                     ((type ...) (generate-temporaries #'(name ...))))
         #'(define-public id
             (let* ((type (type-ref t))
                    ...
                    (pred (%type-predicate type))
                    ...)
               (define rtd (make-record-type 'id '((immutable name) ...)))
               (define raw-constructor (record-constructor rtd))
               (define (typed-constructor name ...)
                 (unless (pred name)
                   (error "bad value for field" 'name name))
                 ...
                 (raw-constructor name ...))
               (product rtd typed-constructor (list type ...)))))))))

(define-syntax-rule (<- t v ...)
  ((type-constructor t) v ...))

;; Pre-declaration to allow recursion.
(define-public instr
  (predicate (lambda (x) ((%type-predicate instr) x))))

(define-type bool (or '#f '#t))
(define-type const (list instr))
(define-public i32 (fixed-width-integer 32 #t))
(define-public u32 (fixed-width-integer 32 #f))
(define-public i64 (fixed-width-integer 64 #t))
(define-public f64 (predicate (lambda (x) (and (real? x) (inexact? x)))))
(define-public name (predicate string?))
(define-public var u32)

(define-type value-type
  (or 'i32 'i64 'f32 'f64))

(define-type result-type
  (list value-type))

(define-struct-type func-type
  (params result-type)
  (results result-type))

(define-type block-type (or '#f value-type var))

(define-struct-type $unary-int-op
  (name (or 'clz 'ctz 'popcnt))
  (type (or 'i32 'i64)))

(define-struct-type $binary-int-op
  (name (or 'add 'sub 'mul 'div_s 'div_u 'rem_s 'rem
            'and 'or 'xor 'shl 'shr_s 'shr_u 'rotl 'rotr))
  (type (or 'i32 'i64)))

(define-struct-type $test-int-op
  (name 'eqz)
  (type (or 'i32 'i64)))

(define-struct-type $compare-int-op
  (name (or 'eq 'ne 'lt_s 'lt_u 'gt_s 'gt_u 'le_s 'le_u 'ge_s 'ge_u))
  (type (or 'i32 'i64)))

(define-struct-type $convert-int-op
  (name (or 'extend_i32_s 'extend_i32_u 'wrap_i64 'trunc_f32_s 'trunc_f32_u
            'trunc_f64_s 'trunc_f64_u 'reinterpret_float
            ;; Sign extension proposal.
            'extend8_s 'extend16_s 'extend32_s))
  (type (or 'i32 'i64)))

(define-struct-type $unary-float-op
  (name (or 'neg 'abs 'ceil 'floor 'trunc 'nearest 'sqrt))
  (type (or 'f32 'f64)))

(define-struct-type $binary-float-op
  (name (or 'add 'sub 'mul 'div 'min 'max 'copysign))
  (type (or 'f32 'f64)))

(define-struct-type $compare-float-op
  (name (or 'eq 'ne 'lt 'gt 'le 'ge))
  (type (or 'f32 'f64)))

(define-struct-type $convert-float-op
  (name (or 'convert_i32_s 'convert_i32_u 'convert_i64_s 'convert_i64_u
            'promote_f32 'demote_f64 'reinterpret_int))
  (type (or 'f32 'f64)))

(define-type $unary-op (or $unary-int-op $unary-float-op))
(define-type $binary-op (or $binary-int-op $binary-float-op))
(define-type $test-op $test-int-op)
(define-type $compare-op (or $compare-int-op $compare-float-op))
(define-type $convert-op (or $convert-int-op $convert-float-op))

(define-struct-type memarg
  (align i32)
  (offset i32))
(define-struct-type $int-memory-op
  (name (or 'load 'store
            'load8_s 'load8_u 'load16_s 'load16_u 'load32_s 'load32_u
            'store8 'store16 'store32))
  (type (or 'i32 'i64))
  (memarg memarg))
(define-struct-type $float-memory-op
  (name (or 'load 'store))
  (type (or 'f32 'f64))
  (memarg memarg))
(define-struct-type $memory-size)
(define-struct-type $memory-grow)
(define-type $memory-op
  (or $int-memory-op $float-memory-op $memory-size $memory-grow))

(define-struct-type $unreachable)
(define-struct-type $nop)
(define-struct-type $drop)
(define-struct-type $select
  (kind (or 'numeric 'typed)))
(define-struct-type $block ;; fixme: circularity
  (type block-type)
  (body (list instr)))
(define-struct-type $loop ;; fixme: circularity
  (type block-type)
  (body (list instr)))
(define-struct-type $if
  (type block-type)
  (then (list instr))
  (else (list instr)))
(define-struct-type $br
  (target var))
(define-struct-type $br-if
  (target var))
(define-struct-type $br-table
  (targets (list var))
  (default var))
(define-struct-type $return)
(define-struct-type $call
  (proc var))
(define-struct-type $call-indirect
  (proc var))
(define-type $call-op
  (or $call $call-indirect))

(define-struct-type $local
  (op (or 'get 'set 'tee))
  (var var))
(define-struct-type $global
  (op (or 'get 'set))
  (var var))
(define-struct-type $table
  (op (or 'get 'set))
  (table var))
(define-type $var-op (or $local $global $table))

(define-struct-type $i32-const (value i32))
(define-struct-type $i64-const (value i64))
(define-struct-type $f32-const (value f64)) ;; FIXME for NaN
(define-struct-type $f64-const (value f64)) ;; FIXME for NaN
(define-type $const-op (or $i32-const $i64-const $f32-const $f64-const))

;; Real definition.
(define-type instr
  (or $unreachable $nop $drop $select
      $block $loop $if $br $br-if $br-table $return
      $call-op $var-op $const-op
      $unary-op $binary-op $test-op $compare-op $convert-op))

(define-struct-type global-type
  (type value-type)
  (mutable? bool))

(define-struct-type global
  (type global-type)
  (value const))

(define-struct-type limits
  (min i32)
  (max (or i32 '#f)))

(define-struct-type table-type
  (limits limits)
  (type 'func-ref))

(define-struct-type table
  (type table-type))

(define-struct-type memory-type
  (limits limits))

(define-struct-type memory
  (type memory-type))

(define-struct-type func
  (type var)
  (locals (list value-type))
  (body (list instr)))

(define-struct-type elem
  (index var)
  (offset const)
  (init (list var)))

(define-struct-type data
  (index var)
  (offset const)
  (init name))

(define-struct-type func-import
  (type var))
(define-struct-type table-import
  (type table-type))
(define-struct-type memory-import
  (type memory-type))
(define-struct-type global-import
  (type global-type))

(define-struct-type import
  (module-name name)
  (item-name name)
  (desc (or func-import
            table-import
            memory-import
            global-import)))

(define-struct-type func-export
  (name var))
(define-struct-type table-export
  (name var))
(define-struct-type memory-export
  (name var))
(define-struct-type global-export
  (name var))

(define-struct-type export
  (name name)
  (desc (or func-export
            table-export
            memory-export
            global-export)))

(define-struct-type module
  (types (list func-type))
  (globals (list global))
  (tables (list table))
  (memories (list memory))
  (funcs (list func))
  (start (or var '#f))
  (elems (list elem))
  (data (list data))
  (imports (list import))
  (exports (list export)))
