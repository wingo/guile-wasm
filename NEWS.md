# User-visible changes in guile-wasm releases

guile-wasm is a WebAssembly toolkit for Guile.

## guile-wasm 0.1.0 -- 2019-11-19

Initial release.
