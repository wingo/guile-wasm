# About

guile-wasm is a WebAssembly toolkit for Guile, providing facilities
assemble and disassemble WebAssembly files.


## Getting guile-wasm

```
git clone https://gitlab.com/wingo/guile-wasm
```

Build it from git like this:

```
autoreconf -ivf
./configure
make
```


## Using guile-wasm

I wrote guile-wasm to help with my work on WebAssembly engines.  I see a
lot of broken files and test cases; this was the motivation for writing
a toolkit, to help me work with these files.  Some things you can do:

```
$ ./env guile
guile> (use-modules (wasm))

;; Parse a WebAssembly file and dump its contents in wat format, in a
;; one-pass, as-we-go kind of a way
guile> (call-with-input-file "foo.wasm" dump)

;; Parse a WebAssembly file and decode it to a Scheme representation
guile> (call-with-input-file "foo.wasm" decode)
```


## Copying guile-wasm

You are free to copy, modify, and redistribute guile-wasm, subject to
the terms of the GPLv3+.  See the COPYING file for more information.


## Contact info

Mailing List: guile-user@gnu.org


## Build dependencies

Guile 2.9.5 or later.
